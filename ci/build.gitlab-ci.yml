.build:linux:base:
  stage: build
  image: ubuntu:18.04
  variables:
    EIGEN_CI_BUILD_TARGET: buildtests
  before_script:
    # Set noninteractive, otherwise tzdata may be installed and prompt for a
    # geographical region.
    - export DEBIAN_FRONTEND=noninteractive
    - export | grep EIGEN
    - apt-get update -y
    - apt-get install -y --no-install-recommends software-properties-common
    - add-apt-repository -y  ppa:ubuntu-toolchain-r/test
    - apt-get update
    - apt-get install --no-install-recommends -y ${EIGEN_CI_CXX_COMPILER}
        ${EIGEN_CI_CC_COMPILER} git cmake ninja-build
    # ONLY FOR CI TESTING: clone into current directory
    - git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo && cp -R eigen_repo/* .
  script:
    - mkdir -p ${EIGEN_CI_BUILDDIR} && cd ${EIGEN_CI_BUILDDIR}
    - cmake -G ${EIGEN_CI_CMAKE_GENERATOR} 
        -DCMAKE_CXX_COMPILER=${EIGEN_CI_CXX_COMPILER}
        -DCMAKE_C_COMPILER=${EIGEN_CI_CC_COMPILER}
        ${EIGEN_CI_ADDITIONAL_ARGS} ..
    # Builds (particularly gcc) sometimes get killed, potentially when running
    # out of resources.  In that case, keep trying to build the remaining
    # targets, then try to build again with a single thread.
    - cmake --build . --target ${EIGEN_CI_BUILD_TARGET} -- -k0 ||
      cmake --build . --target ${EIGEN_CI_BUILD_TARGET} -- -k0 -j1
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - ${EIGEN_CI_BUILDDIR}/
    expire_in: 5 days
  only:
    - schedules
    - web

.build:windows:base:
  stage: build
  variables:
    EIGEN_CI_BUILD_TARGET: buildtests
    EIGEN_CI_MSVC_ARCH: x64
    EIGEN_CI_MSVC_VER: "14.29"
    # Reduce overall build size and compile time. 
    # Note: /d2ReducedOptimizeHugeFunctions is only available in VS 2019.
    EIGEN_CI_TEST_CUSTOM_CXX_FLAGS: "/d2ReducedOptimizeHugeFunctions /DEIGEN_STRONG_INLINE=inline /Os"
    EIGEN_CI_ADDITIONAL_ARGS: ""
    EIGEN_CI_BEFORE_SCRIPT: ""
  before_script:
    # Print configuration variables.
    - Get-Variable | findstr EIGEN
    # Run a custom before-script command.
    - if ("${EIGEN_CI_BEFORE_SCRIPT}") { Invoke-Expression -Command "${EIGEN_CI_BEFORE_SCRIPT}" }
    # ONLY FOR CI TESTING: clone into current directory
    - git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo &&
      robocopy eigen_repo . /s /e > robocopy.log || echo 'copied files.'
  script:
    # Find Visual Studio installation directory.
    - $VS_INSTALL_DIR = &"${Env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -latest -property installationPath
    # Run VCVarsAll.bat initialization script and extract environment variables.
    # http://allen-mack.blogspot.com/2008/03/replace-visual-studio-command-prompt.html
    - cmd.exe /c "`"${VS_INSTALL_DIR}\VC\Auxiliary\Build\vcvarsall.bat`" $EIGEN_CI_MSVC_ARCH -vcvars_ver=$EIGEN_CI_MSVC_VER & set" |
      foreach {
        if ($_ -match "=") {
          $v = $_.split("="); set-item -force -path "ENV:\$($v[0])" -value "$($v[1])"
        }
      }
    - mkdir $EIGEN_CI_BUILDDIR && cd $EIGEN_CI_BUILDDIR
    # We need to split EIGEN_CI_ADDITIONAL_ARGS, otherwise they are interpretted
    # as a single argument.  Split by space, unless double-quoted.
    - $split_args = [regex]::Split(${EIGEN_CI_ADDITIONAL_ARGS}, ' (?=(?:[^"]|"[^"]*")*$)' )
    - cmake -G "${EIGEN_CI_CMAKE_GENERATOR}"
      -DCMAKE_BUILD_TYPE=MinSizeRel
      -DEIGEN_TEST_CUSTOM_CXX_FLAGS="${EIGEN_CI_TEST_CUSTOM_CXX_FLAGS}"
      ${split_args} ..
    # Windows builds sometimes fail due heap errors. In that case, try
    # building the rest, then try to build again with a single thread.
    - cmake --build . --target $EIGEN_CI_BUILD_TARGET -- -k0 ||
      cmake --build . --target $EIGEN_CI_BUILD_TARGET -- -k0 -j1
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      - ${EIGEN_CI_BUILDDIR}/
    expire_in: 5 days
  only:
    - schedules
    - web

######## x86-64 ################################################################
.build:x86-64:linux:base:
  extends: .build:linux:base
  tags:
    - eigen-runner
    - linux
    - x86-64

# GCC-5 (minimum for c++14)
build:x86-64:linux:gcc-5:default:
  extends: .build:x86-64:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-5"
    EIGEN_CI_CC_COMPILER: "gcc-5"

# GCC-9 (default on Ubuntu 20.04 LTS)
build:x86-64:linux:gcc-9:default:
  extends: .build:x86-64:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-9"
    EIGEN_CI_CC_COMPILER: "gcc-9"

# GCC-10
build:x86-64:linux:gcc-10:default:
  extends: .build:x86-64:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

build:x86-64:linux:gcc-10:avx2:
  extends: build:x86-64:linux:gcc-10:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX2=on"
    
build:x86-64:linux:gcc-10:avx512dq:
  extends: build:x86-64:linux:gcc-10:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX512DQ=on"

# Clang-6 (minimum installable on Ubuntu 20.04 LTS)
build:x86-64:linux:clang-6:default:
  extends: .build:x86-64:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-6.0"
    EIGEN_CI_CC_COMPILER: "clang-6.0"

# Clang-9 (default on Ubuntu 20.04 LTS)
build:x86-64:linux:clang-9:default:
  extends: .build:x86-64:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-9"
    EIGEN_CI_CC_COMPILER: "clang-9"

# Clang-12 (latest release)
build:x86-64:linux:clang-12:default:
  extends: .build:x86-64:linux:base
  image: ubuntu:20.04
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"

build:x86-64:linux:clang-12:avx2:
  extends: build:x86-64:linux:clang-12:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX2=on"
    
build:x86-64:linux:clang-12:avx512dq:
  extends: build:x86-64:linux:clang-12:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX512DQ=on"

# Clang sanitizers
build:x86-64:linux:clang-12:default:asan:
  extends: build:x86-64:linux:clang-12:default
  image: ubuntu:20.04
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: 
      -DEIGEN_TEST_CUSTOM_CXX_FLAGS=-fsanitize=address,undefined
      -DEIGEN_TEST_CUSTOM_LINKER_FLAGS=-fsanitize=address,undefined

# build:x86-64:linux:clang-12:default:msan:
#   extends: build:x86-64:linux:clang-12:default
#   image: ubuntu:20.04
#   variables:
#     EIGEN_CI_ADDITIONAL_ARGS: 
#       -DEIGEN_TEST_CUSTOM_CXX_FLAGS=-fsanitize=memory
#       -DEIGEN_TEST_CUSTOM_LINKER_FLAGS=-fsanitize=memory

######## CUDA ##################################################################
.build:x86-64:linux:cuda:base:
  extends: .build:linux:base
  variables:
    # Addtional flags passed to the cuda compiler.
    EIGEN_CI_CUDA_CXX_FLAGS: ""
    # Compute architectures present in the GitLab CI runners.
    EIGEN_CI_CUDA_COMPUTE_ARCH: "50;75"
    EIGEN_CI_BUILD_TARGET: buildtests_gpu
    EIGEN_CI_TEST_CUDA_CLANG: "off"
    EIGEN_CI_ADDITIONAL_ARGS:
      -DEIGEN_TEST_CUDA=on 
      -DEIGEN_CUDA_CXX_FLAGS=${EIGEN_CI_CUDA_CXX_FLAGS}
      -DEIGEN_CUDA_COMPUTE_ARCH=${EIGEN_CI_CUDA_COMPUTE_ARCH}
      -DEIGEN_TEST_CUDA_CLANG=${EIGEN_CI_TEST_CUDA_CLANG}
  tags:
    - eigen-runner
    - linux
    - x86-64

# GCC-7, CUDA-9.2
build:x86-64:linux:gcc-7:cuda-9.2:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:9.2-devel-ubuntu18.04
  variables:
    # cuda 9.2 doesn't support sm_75, so lower to 70.
    EIGEN_CI_CUDA_COMPUTE_ARCH: "50;70"
    EIGEN_CI_CXX_COMPILER: "g++-7"
    EIGEN_CI_CC_COMPILER: "gcc-7"

# Clang-10, CUDA-9.2
build:x86-64:linux:clang-10:cuda-9.2:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:9.2-devel-ubuntu18.04
  variables:
    EIGEN_CI_CUDA_COMPUTE_ARCH: "50;70"
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
    EIGEN_CI_TEST_CUDA_CLANG: "on"

# GCC-8, CUDA-10.2
build:x86-64:linux:gcc-8:cuda-10.2:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:10.2-devel-ubuntu18.04
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-8"
    EIGEN_CI_CC_COMPILER: "gcc-8"

# Clang-10, CUDA-10.2
build:x86-64:linux:clang-10:cuda-10.2:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:10.2-devel-ubuntu18.04
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
    EIGEN_CI_TEST_CUDA_CLANG: "on"
    
# GCC-10, CUDA-11.4
build:x86-64:linux:gcc-10:cuda-11.4:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:11.4.2-devel-ubuntu20.04
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"

# Clang-12, CUDA-11.4
build:x86-64:linux:clang-12:cuda-11.4:
  extends: .build:x86-64:linux:cuda:base
  image: nvidia/cuda:11.4.2-devel-ubuntu20.04
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-12"
    EIGEN_CI_CC_COMPILER: "clang-12"
    EIGEN_CI_TEST_CUDA_CLANG: "on"

# ######## HIP ###################################################################
# Note: these are currently build-only, until we get an AMD-supported runner.

# ROCm HIP
build:x86-64:linux:gcc-10:rocm-latest:
  extends: .build:linux:base
  image: rocm/dev-ubuntu-18.04:latest
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"
    EIGEN_CI_BUILD_TARGET: buildtests_gpu
    EIGEN_CI_ADDITIONAL_ARGS: -DEIGEN_TEST_HIP=on
  tags:
    - eigen-runner
    - linux
    - x86-64

######## AArch64 ###############################################################
# GCC-10
build:aarch64:linux:gcc-10:
  extends: .build:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"
  tags:
    - eigen-runner
    - linux
    - aarch64

# Clang-10
build:aarch64:linux:clang-10:
  extends: .build:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
  tags:
    - eigen-runner
    - linux
    - aarch64

######## ppc64le ###############################################################
# Currently all ppc64le jobs are allowed to fail

# GCC-10
build:ppc64le:linux:gcc-10:
  allow_failure: true
  extends: .build:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "g++-10"
    EIGEN_CI_CC_COMPILER: "gcc-10"
    EIGEN_CI_ADDITIONAL_ARGS: "-DCMAKE_CXX_FLAGS='-DEIGEN_ALTIVEC_DISABLE_MMA'"
  tags:
    - eigen-runner
    - linux
    - ppc64le

# Clang-10
build:ppc64le:linux:clang-10:
  allow_failure: true
  extends: .build:linux:base
  variables:
    EIGEN_CI_CXX_COMPILER: "clang++-10"
    EIGEN_CI_CC_COMPILER: "clang-10"
  tags:
    - eigen-runner
    - linux
    - ppc64le

######### MSVC #################################################################
.build:x86-64:windows:base:
  extends: .build:windows:base
  tags:
    - eigen-runner
    - windows
    - x86-64

# MSVC 14.16 (VS 2017)
build:x86-64:windows:msvc-14.16:default:
  extends: .build:x86-64:windows:base
  variables:
    EIGEN_CI_MSVC_VER: "14.16"
    # Override to remove unsupported /d2ReducedOptimizeHugeFunctions.
    EIGEN_CI_TEST_CUSTOM_CXX_FLAGS: "/DEIGEN_STRONG_INLINE=inline /Os"

# MSVC 14.29 (VS 2019)
build:x86-64:windows:msvc-14.29:default:
  extends: .build:x86-64:windows:base
  variables:
    EIGEN_CI_MSVC_VER: "14.29"
    
build:x86-64:windows:msvc-14.29:avx2:
  extends: build:x86-64:windows:msvc-14.29:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX2=on"

build:x86-64:windows:msvc-14.29:avx512dq:
  extends: build:x86-64:windows:msvc-14.29:default
  variables:
    EIGEN_CI_ADDITIONAL_ARGS: "-DEIGEN_TEST_AVX512DQ=on"

######### MSVC + CUDA ##########################################################
.build:windows:cuda:base:
  extends: .build:windows:base
  variables:
    # Addtional flags passed to the cuda compiler.
    EIGEN_CI_CUDA_CXX_FLAGS: ""
    # Compute architectures present in the GitLab CI runners.
    EIGEN_CI_CUDA_COMPUTE_ARCH: "50;75"
    EIGEN_CI_BUILD_TARGET: buildtests_gpu
    EIGEN_CI_ADDITIONAL_ARGS:
      -DEIGEN_TEST_CUDA=on 
      -DEIGEN_CUDA_CXX_FLAGS="${EIGEN_CI_CUDA_CXX_FLAGS}"
      -DEIGEN_CUDA_COMPUTE_ARCH="${EIGEN_CI_CUDA_COMPUTE_ARCH}"
  tags:
    - eigen-runner
    - windows
    - x86-64
    - cuda
    
# MSVC 14.16 + CUDA 9.2
build:x86-64:windows:msvc-14.16:cuda-9.2:
  extends: .build:windows:cuda:base
  variables:
    # CUDA 9.2 doesn't support sm_75.
    EIGEN_CI_CUDA_COMPUTE_ARCH: "50;70"
    # CUDA 9.2 only supports up to VS 2017.
    EIGEN_CI_MSVC_VER: "14.16"
    EIGEN_CI_TEST_CUSTOM_CXX_FLAGS: "/DEIGEN_STRONG_INLINE=inline /Os"
    EIGEN_CI_BEFORE_SCRIPT: $$env:CUDA_PATH=$$env:CUDA_PATH_V9_2

# MSVC 14.29 + CUDA 10.2
build:x86-64:windows:msvc-14.29:cuda-10.2:
  extends: .build:windows:cuda:base
  variables:
    EIGEN_CI_MSVC_VER: "14.29"
    EIGEN_CI_BEFORE_SCRIPT: $$env:CUDA_PATH=$$env:CUDA_PATH_V10_2

# MSVC 14.29 + CUDA 11.4
build:x86-64:windows:msvc-14.29:cuda-11.4:
  extends: .build:windows:cuda:base
  variables:
    EIGEN_CI_MSVC_VER: "14.29"
    EIGEN_CI_BEFORE_SCRIPT: $$env:CUDA_PATH=$$env:CUDA_PATH_V11_4

# # Cross-compiler tests.
# .build:linux:cross:base:
#   stage: build
#   image: ubuntu:20.04
#   variables:
#     EIGEN_CI_CXX_COMPILER: g++-10
#     EIGEN_CI_CXX_COMPILER_ARCH: x86_64
#     EIGEN_CI_CXX_CROSS_COMPILER_INSTALL: g++-10-x86-64-linux-gnu
#     EIGEN_CI_CXX_CROSS_COMPILER: x86-64-linux-gnu-g++-10
#     EIGEN_CI_CC_CROSS_COMPILER_INSTALL: gcc-10-x86-64-linux-gnu
#     EIGEN_CI_CC_CROSS_COMPILER: x86-64-linux-gnu-gcc-10
#   before_script:
#     # Set noninteractive, otherwise tzdata may be installed and prompt for a
#     # geographical region.
#     - export DEBIAN_FRONTEND=noninteractive
#     - export | grep EIGEN
#     - apt-get update -y
#     - apt-get install -y --no-install-recommends software-properties-common
#     - add-apt-repository -y  ppa:ubuntu-toolchain-r/test
#     - apt-get update
#     - export ARCH=`uname -m`
#     - echo "arch=$ARCH"
#     - apt-get install --no-install-recommends -y git cmake ninja-build
#     - if [ "$ARCH" == "${EIGEN_CI_CXX_COMPILER_ARCH}" ]; then
#         export EIGEN_CI_CXX_COMPILER_INSTALL=${EIGEN_CI_CXX_COMPILER};
#         export EIGEN_CI_CC_COMPILER_INSTALL=${EIGEN_CI_CC_COMPILER};
#       else
#         export EIGEN_CI_CXX_COMPILER=${EIGEN_CI_CXX_CROSS_COMPILER};
#         export EIGEN_CI_CC_COMPILER=${EIGEN_CI_CC_CROSS_COMPILER};
#         export EIGEN_CI_CXX_COMPILER_INSTALL=${EIGEN_CI_CXX_CROSS_COMPILER_INSTALL};
#         export EIGEN_CI_CC_COMPILER=${EIGEN_CI_CC_CROSS_COMPILER_INSTALL};
#       fi
#     - echo ${EIGEN_CI_CXX_COMPILER} ${EIGEN_CI_CXX_COMPILER_INSTALL}
#         ${EIGEN_CI_CC_COMPILER} ${EIGEN_CI_CC_COMPILER_INSTALL}
#     - apt-get install --no-install-recommends -y ${EIGEN_CI_CXX_COMPILER_INSTALL} ${EIGEN_CI_CC_COMPILER_INSTALL}
#     # ONLY FOR CI TESTING: clone into current directory
#     - echo git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo
#     - git clone --depth 1 --branch ${EIGEN_CI_GIT_BRANCH} ${EIGEN_CI_GIT_URL} eigen_repo && cp -R eigen_repo/* .
#     - ls -a eigen_repo
#   script:
#     - echo cmake -G ${EIGEN_CI_CMAKE_GENERATOR} 
#         -DCMAKE_CXX_COMPILER=${EIGEN_CI_CXX_COMPILER}
#         -DCMAKE_C_COMPILER=${EIGEN_CI_CC_COMPILER}
#         ${EIGEN_CI_ADDITIONAL_ARGS} ..
#   only:
#     - schedules
#     - web
    
# build:ppc64le:linux:cross:
#   extends: .build:linux:cross:base
#   tags:
#     - linux
#     - eigen-runner
#     - ppc64le

# build:aarch64:linux:cross:
#   extends: .build:linux:cross:base
#   tags:
#     - linux
#     - eigen-runner
#     - aarch64
    
# build:x86-64:linux:cross:
#   extends: .build:linux:cross:base
#   tags:
#     - linux
#     - eigen-runner
#     - x86-64
